r-cran-s2 (1.1.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Unfuzz debian/patches/fix-armel-ftbfs.patch

 -- Charles Plessy <plessy@debian.org>  Fri, 04 Oct 2024 08:37:27 +0900

r-cran-s2 (1.1.6-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 09 Jan 2024 09:20:15 +0100

r-cran-s2 (1.1.4-1) unstable; urgency=medium

  * New upstream version
  * Test-Depends as per Suggests

 -- Andreas Tille <tille@debian.org>  Fri, 30 Jun 2023 21:49:50 +0200

r-cran-s2 (1.1.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 16 Jan 2023 13:49:37 +0100

r-cran-s2 (1.1.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 28 Nov 2022 15:28:30 +0100

r-cran-s2 (1.1.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 21 Jul 2022 17:46:21 +0200

r-cran-s2 (1.0.7-3) unstable; urgency=medium

  * d/p/fix-armel-ftbfs.patch
    + Extend patch to fix FTBFS on mipsel, m68k, powerpc and riscv64
  * Add myself to uploaders
  * [skip ci] d/salsa-ci.yml: Disable reprotest

 -- Nilesh Patra <nilesh@debian.org>  Wed, 06 Oct 2021 16:08:58 +0530

r-cran-s2 (1.0.7-2) unstable; urgency=medium

  * Team Upload.
  * Add patch to fix FTBFS on armel

 -- Nilesh Patra <nilesh@debian.org>  Tue, 05 Oct 2021 16:34:37 +0530

r-cran-s2 (1.0.7-1) unstable; urgency=medium

  [ Nilesh Patra ]
  * Team upload.
  * New upstream version 1.0.7
  * d/control: Update versioned B-D
  * d/copyright: Fix copyright files for absl to right location

  [ Étienne Mollier ]
  * add fix-ftbfs-on-32bit.patch from Konstantin Podsirov

 -- Étienne Mollier <emollier@debian.org>  Mon, 04 Oct 2021 21:37:06 +0200

r-cran-s2 (1.0.6-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 26 Aug 2021 12:48:38 +0200

r-cran-s2 (1.0.4-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 16 Jan 2021 17:30:37 +0100

r-cran-s2 (1.0.3-2) unstable; urgency=medium

  * change uint64 to uint64_t in unaligned_access
    Closes: #976473
  * Standards-Version: 4.5.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 10 Dec 2020 08:26:41 +0100

r-cran-s2 (1.0.3-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Sun, 18 Oct 2020 23:03:45 +0200

r-cran-s2 (1.0.2-1) unstable; urgency=medium

  * Initial release (closes: #970713)

 -- Andreas Tille <tille@debian.org>  Tue, 22 Sep 2020 11:28:09 +0200
